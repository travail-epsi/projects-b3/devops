CREATE DATABASE IF NOT EXISTS cooking_book;
USE cooking_book;
CREATE TABLE recipe(
  Id_recipe COUNTER,
  name_recipe VARCHAR(50),
  desc_recipe TEXT,
  PRIMARY KEY(Id_recipe)
);
CREATE TABLE item(
  Id_item COUNTER,
  item_name VARCHAR(50),
  PRIMARY KEY(Id_item)
);
CREATE TABLE compose(
  Id_recipe INT,
  Id_item INT,
  quantity DECIMAL(15, 2),
  PRIMARY KEY(Id_recipe, Id_item),
  FOREIGN KEY(Id_recipe) REFERENCES recipe(Id_recipe),
  FOREIGN KEY(Id_item) REFERENCES item(Id_item)
);
